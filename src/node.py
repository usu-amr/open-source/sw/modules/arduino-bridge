#!/usr/bin/env python
import rospy
from std_msgs.msg import Bool, Float32MultiArray, Float32, Int32
import math
import importlib
from serial import Serial, SerialException
import time
from cobs import cobs
import struct

serial_port = None
zeroByte = b'\x00' # COBS 1-byte delimiter is hex zero as a (binary) bytes character

# Message
class Message:
    address = 0
    length = 0
    data = None
    def __init__(self, address, length, data):
        self.address = address
        self.length = length
        self.data = data

# Send a message to our friend arduino
def dispatchMessage(message, publishers):
    if message.address == 0x10:
        publishers['kill_switch'].publish(Bool(struct.unpack('B',message.data[0])[0]))
    elif message.address == 0x11:
        publishers['pressure'].publish(Float32(struct.unpack('H',message.data[0:message.length])[0]*10))
    elif message.address == 0x01:
        publishers['arduino_time'].publish(Int32(struct.unpack('B',message.data[0:message.length])[0]))
    else:
        print("Received message: ")
        print(message.address)
        print(message.length)
        print(message.data.encode('hex'))

# Parse a message from the Arduino
def parseMessage(message, publishers):
    n = len(message)
    if n <= 3 and n > 0:
        print('Bad message')
        print(message.encode('hex'))
    if n > 3:
        try:
            res = cobs.decode(message) # recover binary data encoded on Arduino
            address = struct.unpack('B',res[0])[0]
            length = struct.unpack('B',res[1])[0]
            if length+2 == len(res):
                data = res[2:length+2]
                dispatchMessage( Message(address, length, data), publishers )
        except cobs.DecodeError as e:
            print(str(e))

# Send a message over a serial port to somewhere?
def write_to_port(serial_port, address, data):
    try:
        if serial_port is not None and serial_port.is_open:
            ba = bytearray(struct.pack("B", address))
            ba.append(struct.pack("B", len(data)))
            ba += data
            cobs_data = cobs.encode(str(ba))
            serial_port.write(cobs_data)
            serial_port.write(b'\x00')                
    except SerialException as e:
        print("Serial Exception", str(e))
        if serial_port is not None:
            serial_port.close()

# Send some sort of message over the serial port about motors, sure wish all this stuff was commented before
def on_cmd_motors(data):
    ba = bytearray(0)
    for dat in data.data:
        datBytes = struct.pack("h", dat*10000)
        b1 = datBytes[0]
        b2 = datBytes[1]
        ba.append(b1)
        ba.append(b2)
    write_to_port(serial_port, 0x1E, ba)

# Callback for a ROS echo command?
def on_echo(data):
    write_to_port(serial_port, 0, struct.pack("B", data.data))

# I believe this is the entry point for the code...
def node(publishers):
    rate = rospy.Rate(100)
    rospy.loginfo('Ready')
    message = b''
    data = b''
    while not rospy.is_shutdown():
        bytecount = 0
        data = b''
        # While the ROBOT is connected to a serial port read data and 
        while serial_port.is_open and serial_port.inWaiting() and data != b'\x00' and bytecount < 30:
            # Read bytes until we reach '\x00'
            data = serial_port.read()
            if data == b'\x00':
                # Parse message and dispatch it to publishers
                parseMessage(message, publishers)
                message = b''
            else:
                message = message + data
            bytecount += 1
        # Raise exception if serial port connection is closed
        if not serial_port.is_open:
            raise SerialException("Serial port is closed")
        rate.sleep()

"""
    Entry point
    Opens serial port to the arduino
    Trys to recover from SerialException by waiting 5 seconds and calling main again.
"""
def main(publishers):
    try:
        global serial_port
        serial_port = Serial(rospy.get_param('~port','/dev/ttyS5'), baudrate=rospy.get_param('~baud',115200), bytesize=8, parity='N', stopbits=1, timeout=10)
        node(publishers)
    except SerialException as e:
        print("Serial Exception", str(e))
        if serial_port is not None:
            serial_port.close()
        time.sleep(5)
        main(publishers)

if __name__ == '__main__':
    try:
        rospy.init_node('arduino_bridge')
        rospy.loginfo('Arduino Bridge Loading')
        publishers = {
            'kill_switch': rospy.Publisher("/kill_switch", Bool, queue_size=10),
            'pressure': rospy.Publisher("/pressure", Float32, queue_size=1),
            'arduino_time': rospy.Publisher("/arduino_time", Int32, queue_size=1)
        }
        rospy.Subscriber("/cmd_motors", Float32MultiArray, on_cmd_motors, queue_size=2)
        rospy.Subscriber("/cmd_echo", Int32, on_echo, queue_size=2)
        print('Starting connection')
        main(publishers)
    except rospy.ROSInterruptException:
        pass
